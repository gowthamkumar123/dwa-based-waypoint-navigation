#include<iostream>
#include<vector>
#include<array>
#include<cmath>
#include <unordered_map>
#include<map>
#include<opencv2/opencv.hpp>
#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>

using Traj = std::vector<std::array<float, 5>>;
using Obstacle = std::vector<std::array<float, 2>>;
using State = std::array<float, 5>;
using Window = std::array<float, 4>;
using Point = std::array<float, 2>;
using Control = std::array<float, 2>;

using namespace std;

//tuning parameters 
struct Config{
public:
  float max_speed = 0.5;
  float min_speed = -0.3;
  float max_yawrate = 40.0 * M_PI / 180.0;
  float max_accel = 0.1;
  float robot_radius = 0.5;
  float max_dyawrate = 40.0 * M_PI / 180.0;

  float v_reso = 0.01;
  float yawrate_reso = 0.1 * M_PI / 180.0;

  float dt = 0.1;
  float predict_time = 3.0;
  float to_goal_cost_gain = 1.0;
  float speed_cost_gain = 1.0;
};

float ManhattanDistance(pair<float,float> wpStart,pair<float,float> wpCurr){
  return abs(wpStart.first - wpCurr.first) + abs(wpStart.second - wpCurr.second);
}

float EuclideanDistance(pair<float,float> wpStart,pair<float,float> wpCurr){
  return sqrt(pow(wpStart.first - wpCurr.first,2) + pow(wpStart.second - wpCurr.second,2));
}

array<float,5> motion(State CurrState, Control Actions, float dt){
  CurrState[2] += Actions[1] * dt;
  CurrState[0] += Actions[0] * std::cos(CurrState[2]) * dt;
  CurrState[1] += Actions[0] * std::sin(CurrState[2]) * dt;
  CurrState[3] = Actions[0];
  CurrState[4] = Actions[1];
  return CurrState;
};

//dynamic window is generated
array<float, 4> calc_dynamic_window(State x, Config config){
  return {{
    std::max((x[3] - config.max_accel * config.dt), config.min_speed),
    std::min((x[3] + config.max_accel * config.dt), config.max_speed),
    std::max((x[4] - config.max_dyawrate * config.dt), -config.max_yawrate),
    std::min((x[4] + config.max_dyawrate * config.dt), config.max_yawrate)
  }};
};

vector<std::array<float,5>> find_path(State x, float v, float y, Config config){
  Traj traj;
  traj.push_back(x);
  float time = 0.0;
  while (time <= config.predict_time){
    x = motion(x, std::array<float, 2>{{v, y}}, config.dt);
    traj.push_back(x);
    time += config.dt;
  }
  return traj;
};

//obstacle cost is calculated 
float calc_obstacle_cost(Traj traj, Obstacle ob, Config config){
  // calc obstacle cost inf: collission, 0:free
  int skip_n = 2;
  float minr = std::numeric_limits<float>::max();

  for (unsigned int i=0; i<traj.size(); i+=skip_n){
    for (unsigned int j=0; j< ob.size(); j++){
      float ox = ob[j][0]; //obstacle x: position
      float oy = ob[j][1]; //obstacle y: position
      float dx = traj[i][0] - ox; //traj x diff v/s obst x diff
      float dy = traj[i][1] - oy; //traj y diff v/s obst y diff

      float r = std::sqrt(dx*dx + dy*dy); //checking for the obstacle distance from the trajectory
      if (r <= config.robot_radius){
          return std::numeric_limits<float>::max(); //return infinite value
      }

      if (minr >= r){
          minr = r; //else return free from collision
      }
    }
  }

  return 1.0 / minr;
};

float cost_to_goal(Traj traj, Point goal, Config config){
  float goal_magnitude = std::sqrt(goal[0]*goal[0] + goal[1]*goal[1]);
  float traj_magnitude = std::sqrt(std::pow(traj.back()[0], 2) + std::pow(traj.back()[1], 2));
  float dot_product = (goal[0] * traj.back()[0]) + (goal[1] * traj.back()[1]);
  float error = dot_product / (goal_magnitude * traj_magnitude);
  float error_angle = std::acos(error);
  float cost = config.to_goal_cost_gain * error_angle;

  return cost;
};

vector<std::array<float, 5>> calc_final_input(
  State x, Control& u,
  Window dw, Config config, Point goal,
  std::vector<std::array<float, 2>>ob){

    float min_cost = 10000.0;
    Control min_u = u;
    min_u[0] = 0.0;
    Traj best_traj;

    // evalucate all trajectory with sampled input in dynamic window
    for (float v=dw[0]; v<=dw[1]; v+=config.v_reso){
      for (float y=dw[2]; y<=dw[3]; y+=config.yawrate_reso){

        Traj traj = find_path(x, v, y, config);

        float to_goal_cost = cost_to_goal(traj, goal, config);
        float speed_cost = config.speed_cost_gain * (config.max_speed - traj.back()[3]);
        float ob_cost = calc_obstacle_cost(traj, ob, config);
        float final_cost = to_goal_cost + speed_cost + ob_cost;

        if (min_cost >= final_cost){
          min_cost = final_cost;
          min_u = Control{{v, y}};
          best_traj = traj;
        }
      }
    }
    u = min_u;
    return best_traj;
};


vector<std::array<float, 5>> dwa_control(State x, Control & u, Config config,
  Point goal, Obstacle ob){
    Window dw = calc_dynamic_window(x, config);
    Traj traj = calc_final_input(x, u, dw, config, goal, ob);

    return u, traj;
  }

cv::Point2i cv_offset(
    float x, float y, int image_width=2000, int image_height=2000){
  cv::Point2i output;
  output.x = int(x * 100) + image_width/2;
  output.y = image_height - int(y * 100) - image_height/3;
  return output;
};

int main(){
  
  //All the trajectory points that are generated to reache 
  //a particular way point could be used as way point which are obstacle free on the grid

  //The initial position of the robot is at {-1,-1}
  vector<pair<float,float>> wypoints;
  map<pair<float, float>, pair<float,float>> umap;

  //umap contains the all the way points with its manhattan and euclidean distance from the starting way points which is (-1,-1)#
  //wypoints contains all way points that the robot has taken to go from one way point to other way point along its path without hitting the obstacle

  State x({{-1, -1, M_PI/8.0, 0.0, 0.0}});
  vector<pair<float,float>> goal2 = {{3,3},{4,4},{-5,2},{-4,3}};
  vector<pair<float,float>>:: iterator itr;
  Obstacle ob({
    {{3,7}},
    {{0, 4}},{0, 8},{0,3},{2,4},{-6,6},{7,7}
  });

bool goalReached = false;
for(itr = goal2.begin();itr!=goal2.end();itr++)
{
  float a = itr->first;
  float b = itr->second;
  Point goal({{a,b}});
  Control u({{0.0, 0.0}});
  Config config;
  Traj traj;
  traj.push_back(x);

  for(int i=0; i<1000 && !goalReached; i++){
    pair<float,float> goalPoint = {a,b};
    pair<float,float> currPoint = {-1,-1};

    float man_dist = ManhattanDistance(goalPoint,currPoint);
    float eucli_dist = EuclideanDistance(goalPoint,currPoint);
    pair<float,float> distAll = make_pair(man_dist,eucli_dist);
    pair<float,float> gwpoint = make_pair(a,b);
    umap.insert(make_pair(gwpoint, distAll));

    cout<<"manhattan_distnace: "<<man_dist<<endl;
    cout<<"Euclidean distance: "<<eucli_dist<<endl;
    Traj ltraj = dwa_control(x, u, config, goal, ob);
    x = motion(x, u, config.dt);
    traj.push_back(x);
   
    // visualization concept source internet
    cv::Mat bg(1200,1500, CV_8UC3, cv::Scalar(200,200,255));
    cv::circle(bg, cv_offset(goal[0], goal[1], bg.cols, bg.rows),
               30, cv::Scalar(255,0,0), 5);

    for(unsigned int j=0; j<ob.size(); j++){
      cv::circle(bg, cv_offset(ob[j][0], ob[j][1], bg.cols, bg.rows),
                 5+j*5, cv::Scalar(0,0,0), -1);    
    }

    for(unsigned int j=0; j<ltraj.size(); j++){
      cv::circle(bg, cv_offset(ltraj[j][0], ltraj[j][1], bg.cols, bg.rows),
                 7, cv::Scalar(0,255,0), -1);

      pair<float,float> trajPoint = {ltraj[j][0],ltraj[j][1]};
      float dist_from_trajectory = EuclideanDistance(goalPoint,trajPoint);
      wypoints.push_back(trajPoint);

      //this part of the code checks the distnace of the goal from the trajectory to keep a threshold distance from the goal and stop before it
      if(dist_from_trajectory<=0.22)
      {
        cout<<"very close to goal"<<endl;
        goalReached = true;
      }
    }

    cv::circle(bg, cv_offset(x[0], x[1], bg.cols, bg.rows),
               30, cv::Scalar(0,0,255), 5);

    if(eucli_dist <= config.robot_radius){ //Here the euclidean distance is checked to see whether the robot has reached the goal location
      for(unsigned int j=0; j<traj.size(); j++){
        cv::circle(bg, cv_offset(traj[j][0], traj[j][1], bg.cols, bg.rows),
                    7, cv::Scalar(0,0,255), -1);
      }
      cout<<"goal reached"<<endl;
      break;
    }

  cv::imshow("Dwa local planner", bg);
	cv::waitKey(7);

  }
  goalReached = false;
  x = {{itr->first, itr->second, M_PI/8.0, 0.0, 0.0}}; //Here the robot initial position is set to goal location the robot has recently reached and next goal location starts from here assumed to be
}
}
