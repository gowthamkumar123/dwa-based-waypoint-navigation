CC = g++
PROJECT = dwa_local_planner
SRC = dwa_local_planner.cpp
LIBS = `pkg-config --cflags --libs opencv4`
$(PROJECT) : $(SRC)
	$(CC) $(SRC) -o $(PROJECT) $(LIBS)