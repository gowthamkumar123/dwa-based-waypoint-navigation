# Way point navigation

## To Build the project
```
git clone https://gitlab.com/gowthamkumar123/dwa-based-waypoint-navigation.git
cd dwa-based-waypoint-navigation
$ make
$ ./dwa_local_planner
```

#### Reference
http://wiki.ros.org/dwa_local_planner 

https://www.ri.cmu.edu/pub_files/pub1/fox_dieter_1997_1/fox_dieter_1997_1.pdf